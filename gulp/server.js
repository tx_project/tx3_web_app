'use strict';

var path = require('path');
var gulp = require('gulp');
var conf = require('./conf');

var browserSync = require('browser-sync');
var browserSyncSpa = require('browser-sync-spa');
var util = require('util');
var proxy = require('proxy-middleware');
var url = require('url');

/**
 * @description 服务端接口
 */
var proxyOptions;
var proxyOptions2;
function environment(target) {
	proxyOptions = url.parse(target + '/admin_api');
  proxyOptions.route = '/admin_api';
  proxyOptions.protocol = 'http:';
}
function environment2(target) {
	proxyOptions2 = url.parse(target + '/api');
	proxyOptions2.route = '/api';
}


function browserSyncInit(baseDir, browser) {
  browser = browser === undefined ? 'default' : browser;

  var routes = null;
  if(baseDir === conf.paths.src || (util.isArray(baseDir) && baseDir.indexOf(conf.paths.src) !== -1)) {
    routes = {
      '/bower_components': 'bower_components'
    };
  }

  var server = {
    baseDir: baseDir,
    routes: routes,
    middleware: [
      proxy(proxyOptions)
      // proxy(proxyOptions2)
    ]
  };

  /*
   * You can add a proxy to your backend by uncommenting the line below.
   * You just have to configure a context which will we redirected and the target url.
   * Example: $http.get('/users') requests will be automatically proxified.
   *
   * For more details and option, https://github.com/chimurai/http-proxy-middleware/blob/v0.9.0/README.md
   */

  browserSync.instance = browserSync.init({
    startPath: '/',
    server: server,
    browser: browser
  });
}

browserSync.use(browserSyncSpa({
  selector: '[ng-app]'// Only needed for angular apps
}));

gulp.task('serve', ['watch'], function () {
  environment('http://192.168.0.45:8088');
  // environment('http://192.168.0.26:8088');
  // environment('http://192.168.1.118:8088');
  // environment('http://192.168.1.102:8088');
  // environment('http://192.168.0.26:8088');
  // environment('http://192.168.0.26:8080');
  // environment('http://192.168.1.108:8088');
  // environment('http://192.168.1.102:8088');
  // environment('http://192.168.1.108:8088');
  // environment('http://192.168.1.108:8088');
  // environment('http://192.168.1.102:8088');
  // environment('http://192.168.43.73:8088');
  // environment('http://192.168.1.104:8088');
  // environment('http://192.168.1.110:8088');

  // environment('http://192.168.1.137:8080');
  browserSyncInit([path.join(conf.paths.tmp, '/serve'), conf.paths.src]);
});

gulp.task('serve:dist', ['build'], function () {
  browserSyncInit(conf.paths.dist);
});

gulp.task('serve:e2e', ['inject'], function () {
  browserSyncInit([conf.paths.tmp + '/serve', conf.paths.src], []);
});

gulp.task('serve:e2e-dist', ['build'], function () {
  browserSyncInit(conf.paths.dist, []);
});
