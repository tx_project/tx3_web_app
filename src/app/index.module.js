import '../app/config/localZh';
import './PUIComponents';

import './lib/util/lodash.min';

import PUIController from './PUIComponents/controller';
import { config } from './config/index.config';
import { routerConfig } from './config/index.route';

import { HeaderDirective } from '../app/components/header/head';
import { MainController } from './main/main.controller'
import { runBlock } from './index.run';

angular.module('tx3WebApp', 
  [
    'ngAnimate',
		'ngCookies',
		'ngTouch',
		'ngSanitize',
		'ngMessages',
		'ngAria',
		'restangular',
		'ui.router',
		'ngLocale',
    'puiComponents'
  ])
  .config(config)
  .config(routerConfig)
  
  .directive('header', HeaderDirective)

  .controller('MainController', MainController)
  .controller('PUIController', PUIController)
  
  .run(runBlock);
