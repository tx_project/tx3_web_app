'use strict';
export function HeaderDirective() {
    'ngInject';

    let directive = {
        restrict: 'EA',
        scope: {},
        templateUrl: 'app/components/header/index.html',
        controller: HeaderController,
        controllerAs: 'vm',
        bindToController: true
    };

    return directive;
}

class HeaderController {
    constructor() {
        'ngInject';
    }
}