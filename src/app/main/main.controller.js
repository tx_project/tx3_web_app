'use strict';
export class MainController {
    constructor($injector) {
        'ngInject';
        this.inject = $injector;
        this.init();
    }

    init() {
        this.inject.get('$timeout')(() => {
            angular.element('.jq_main').addClass('animate-init');
        }, 1000);
        
    }
}