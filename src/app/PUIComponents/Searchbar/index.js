export default function ($timeout) {
    'ngInject';

    let linkFuc = (scope, element, attr, ngModelController) => {
        scope.$open = false;
        scope.$maxWidth = scope.maxWidth || '128';

        ngModelController.$render = function () {
            scope.value = ngModelController.$viewValue;

            if (scope.value) {
                scope.$open = true;

                setTimeout(() => {
                    $(element).find('input').focus();
                }, 30);
            }
        };

        scope.$watch('value', function (newValue) {
            ngModelController.$setViewValue(scope.value);
        });

        scope.$toggle = function (flag) {
            scope.$open = flag;

            if (flag) {
                setTimeout(() => {
                    $(element).find('input').focus();
                }, 30);
            }
        };

        scope.$onFocus = function ($event) {
            $(element).addClass('focus');
            attr.onFocus && scope.onFocus({$event});
        };

        scope.$onBlur = function ($event) {
            $(element).removeClass('focus');

            if (!scope.value) {
                scope.$open = false;
            }
            attr.onBlur && scope.onBlur({$event});
        };

        scope.$onKeyDown = function ($event) {
            const keyCode = $event.which;

            if (keyCode === 13) {
                scope.$onSearch($event);
            }
        };

        scope.$onSearch = function ($event) {
            attr.onSearch && scope.onSearch({$event});
        };
    };

    let directive = {
        restrict: 'EA',
        replace: true,
        require: 'ngModel',
        scope: {
            maxWidth: '@',
            size: '@',
            type: '@',
            placeholder: '@',
            onFocus: '&',
            onBlur: '&',
            onSearch: '&'
        },
        templateUrl: 'app/PUIComponents/Searchbar/template.html',
        link: linkFuc
    };

    return directive;
}
