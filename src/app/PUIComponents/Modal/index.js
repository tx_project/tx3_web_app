export default function ($modal, $q) {
    'ngInject';

    return {
        confirm(config = {}) {
            const defer = $q.defer();

            const modal = $modal({
                template: 'app/PUIComponents/Modal/confirm.tpl.html',
                placement: 'center',
                title: config.title || '提示',
                content: config.content || '',
                backdrop: 'static'
            });

            modal.$scope.hasCancel = angular.isDefined(config.hasCancel) ? config.hasCancel : true;

            modal.$scope.ok = function () {
                defer.resolve();
                modal.hide();
                modal.destroy();
            };

            modal.$scope.dismiss = function () {
                defer.reject();
                modal.hide();
                modal.destroy();
            };

            return defer.promise;
        }
    }
}
