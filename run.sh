case $1 in
    init) #初始化项目 sh run.sh init
        npm install
        bower install
        npm install -g gulp
        gulp serve
        ;;
    start) #启动项目 sh run.sh start
        gulp serve
        ;;
    build) #构建项目 sh run.sh build
        gulp
        ;;
    pull) #提交代码 sh run.sh commit message
        git pull origin develop
        git status
        ;; 
    push)
        git push origin develop
        ;;
    *)
        ;;
esac 